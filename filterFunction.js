const arrayColor = [
  'rojo',
  'azul',
  'amarillo',
  'verde',
  'rojo',
  'amarillo',
  'rojo'
]

// {
//   rojo: ['rojo', 'roj'],
//   amarillo: ['amarillo']
// }

// Eliminar colores repetidos
const uniqueColors = [...new Set(arrayColor)]

// Pasar los colores, unot con el número de veces que se repite
const reduceColors = arrayColor.reduce((acum, color) => {
  if (uniqueColors.includes(color)) {
    const currentColor = acum[color] || []
    acum = {
      ...acum,
      [color]: [...currentColor, color]
    }
  }
  return acum
}, {})

const keyColors = Object.keys(reduceColors)

keyColors.forEach((arrayColor) => {
  const arrayElements = reduceColors[arrayColor]
  console.log(arrayElements, arrayElements.length)
})
