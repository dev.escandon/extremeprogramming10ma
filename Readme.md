# Error al instalar husky

El problema que pude encontrar es que la última versión de husky no funcionaba de manera correcta, para ello instalar la siguiente versión: **^4.3.6**.

```js
npm install -D husky@4.3.6
```

Cuando instalen husky, inicien su repositorio primero y después instalan dicha depedencia. 

En caso de encontrarse con algún error, seguir la siguiente liga: [Error husky](https://github.com/typicode/husky/issues/326)

## Error de jest con standard

Estandar genera un error cuando instalas jest, por lo que la documentación te recomienda agregar lo siguiente: 

``` js
standard --fix --env jest
```

## Componentes de React

```JSX
// Commons
<Input
idInput='idName'
name='nombre'
type='text'
placeholder='Nombre'
classContainerInput=''
classInput=''
ariaDescribedBy='Nombre del usuario'
isRequired={false}
isDisabled={false}
autoComplete='off'
maxLength
minLength
pattern
min
max
/>

<TextArea
idInput="mensajeID"
titleLabelInput=""
name="mensaje"
classTextArea="input is-normal border-radius-30 has-background-white has-text-primary"
isDisabled=""
readOnly=""
isRequired=""
rows="1"
value=""
placeHolder = ""
maxLength = ""
/>
``` 