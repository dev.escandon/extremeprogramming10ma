const shoppingList = [
  'diapers',
  'kleenex',
  'trash bags',
  'paper towels',
  'milk'
]

const funcionSuma = (numeroUno, numeroDos) => {
  return numeroUno + numeroDos
}

test('La suma de los números no coincide', () => {
  expect(funcionSuma(6, 7)).toBe(10)
})

test('La suma de los números coincide', () => {
  expect(funcionSuma(6, 7)).toBe(13)
})

test('the shopping list has milk on it', () => {
  expect(shoppingList).toContain('milk')
})

test('the shopping list has not milk on it', () => {
  expect(shoppingList).toContain('milk 2')
})
