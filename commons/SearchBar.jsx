import React from 'react'
import Image from './Image'
import Input from './Input'

const SearchBar = () => {
  return (
    <div className='filed'>
      <div className='control has-icons-left'>
        <Input
          id='searchBarId'
          name='SearchBar'
          type='text'
          placeholder='Buscar'
          classContainerInput=''
          classInput='input is-normal border-radius-30 has-background-white has-text-grey'
          ariaDescribeBy='Input de búsqueda'
          isRequired={false}
          isDisabled={false}
          autoComplete='off'
        />
        <span>
          <Image
            pathImage='/icon/search.svg'
            altImage='Icono de búsqueda'
            classImage='is-rounded'
            classDivImage='icon is-small is-left'
          />
        </span>
      </div>
    </div>
  )
}
export default SearchBar
