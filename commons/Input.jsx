// component pass ✅
import React from 'react'
import PropTypes from 'prop-types'
const stringPropTypes = PropTypes.string.isRequired
const booleanPropTypes = PropTypes.bool.isRequired

const Input = ({
  classContainerInput,
  classInput,
  ariaDescribeBy,
  isRequired,
  isDisabled,
  ...props
}) => {
  const onChange = (event) => {
    if (props.onChange) props.onChange(event)
  }

  const onKeyPressInput = (event) => {
    if (props.onKeyPressInput) props.onKeyPressInput(event)
  }

  return (
    <div className={`field ${classContainerInput}`}>
      <div className='control'>
        <input
          className={`${classInput}`}
          aria-required={isRequired}
          aria-describedby={ariaDescribeBy} // Descriptive text about the function of the input
          disabled={isDisabled}
          required={isRequired}
          {...props}
          onChange={onChange}
          onKeyPress={onKeyPressInput}
        />
      </div>
    </div>
  )
}

Input.propTypes = {
  classContainerInput: stringPropTypes,
  classInput: stringPropTypes,
  ariaDescribeBy: stringPropTypes,
  isRequired: booleanPropTypes,
  isDisabled: booleanPropTypes
}

export default Input
