// component pass ✅
import React from 'react'
import PropTypes from 'prop-types'
const stringPropTypes = PropTypes.string.isRequired

const Skeleton = ({ isWidth, isHeight }) => {
  const skeletonStyle = {
    width: isWidth,
    height: isHeight
  }
  return <span className='skeleton-box' style={skeletonStyle} />
}

Skeleton.propTypes = {
  isWidth: stringPropTypes,
  isHeight: stringPropTypes
}

export default Skeleton
