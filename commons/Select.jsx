import React from 'react'

const Select = ({
  label,
  classSelect,
  ariaDescribedBy,
  idSelect,
  isDisabled,
  helperClass,
  classContainerInput,
  options,
  keyNameOption,
  keyNameValue,
  ...props
}) => {
  const optionsRender =
    options && options.length !== 0
      ? options.map((itemOption) => {
          return (
            <option
              key={itemOption[keyNameValue]}
              value={itemOption[keyNameValue]}
            >
              {itemOption[keyNameOption]}
            </option>
          )
        })
      : null

  const onChangeSelect = (event) => {
    if (props.onChangeSelect) props.onChangeSelect(event)
  }

  return (
    <div className={`field ${classContainerInput}`}>
      <label
        id={`${idSelect}Label`}
        className={`label ${classSelect}`}
        htmlFor={idSelect}
      >
        {label}
        <div className='control'>
          <div className='select is-full-width'>
            <select
              id={idSelect}
              className={`select ${classSelect} is-full-width`}
              aria-labelledby={`${label}Label`}
              aria-describedby={ariaDescribedBy}
              disabled={isDisabled}
              onChange={onChangeSelect}
              {...props}
            >
              <option value=''>Seleccione una opción</option>
              {optionsRender}
            </select>
          </div>
        </div>
      </label>
    </div>
  )
}

export default Select
